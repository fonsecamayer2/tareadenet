﻿using Nomina.Business.Entities;
using Nomina.Business.Implements;
using Nomina.Views.Extensiones;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Nomina.Views
{
    public partial class FrmEmpleado : Form
    {
        public DataSet DSNomina { get; set; }
        private DaoEmpleadoImplements daoEmpleadoImplements;
        private DataRow drEmpleado;
        private int index;
        private int idActualizar;

        public FrmEmpleado()
        {
            daoEmpleadoImplements = new DaoEmpleadoImplements();
            InitializeComponent();
            
        }

        public void setEmpleado(Empleado e, int index)
        {
            txtApellidos.Text = e.Apellidos;
            txtNombres.Text = e.Nombres;
            txtCedula.Text = e.Cedula;
            txtDireccion.Text = e.Direccion;
            txtTelefono.Text = e.Telefono;
            txtSalario.Text = e.Salario.ToString();
            dateTimePicker1.Value = e.FechaContratacion;
            this.index = index;
            this.idActualizar = e.Id;
            
        }

        public void setEnableButton(bool flag1, bool flag2)
        {
            button1.Enabled = flag1;
            button2.Enabled = flag2;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            drEmpleado = DSNomina.Tables["Empleado"].NewRow();

            if (txtApellidos.Text.Equals("") || txtNombres.Text.Equals("") || txtCedula.Text.Equals("") 
                || txtDireccion.Text.Equals("") || txtSalario.Text.Equals("") || txtTelefono.Text.Equals(""))
            {
                MessageBox.Show("No debe dejar ningun campo vacio", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            Empleado empleado = new Empleado()
            {
                Id = daoEmpleadoImplements.getGuardados()+1,
                Nombres = txtNombres.Text,
                Apellidos = txtApellidos.Text,
                Cedula = txtCedula.Text,
                Telefono = txtTelefono.Text,
                Direccion = txtDireccion.Text,
                Salario = Convert.ToDecimal(txtSalario.Text),
                FechaContratacion = dateTimePicker1.Value
            };
            drEmpleado["Cedula"] = empleado.Cedula;
            drEmpleado["Nombres"] = empleado.Nombres;
            drEmpleado["Apellidos"] = empleado.Apellidos;
            drEmpleado["Direccion"] = empleado.Direccion;
            drEmpleado["Telefono"] = empleado.Telefono;
            drEmpleado["FechaContratacion"] = empleado.FechaContratacion;
            drEmpleado["Salario"] = empleado.Salario;
            drEmpleado["Id"] = empleado.Id;
            if (daoEmpleadoImplements.findByCedula(empleado.Cedula) != null)
            {
                MessageBox.Show("Error, Cedula repetida", "Mensaje de error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            daoEmpleadoImplements.Create(empleado);
            DSNomina.Tables["Empleado"].Rows.Add(drEmpleado);
            Dispose();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            drEmpleado = DSNomina.Tables["Empleado"].NewRow();

            if (txtApellidos.Text.Equals("") || txtNombres.Text.Equals("") || txtCedula.Text.Equals("")
               || txtDireccion.Text.Equals("") || txtSalario.Text.Equals("") || txtTelefono.Text.Equals(""))
            {
                MessageBox.Show("No debe dejar ningun campo vacio", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            Empleado empleado = new Empleado()
            {
                Id = idActualizar,
                Nombres = txtNombres.Text,
                Apellidos = txtApellidos.Text,
                Cedula = txtCedula.Text,
                Telefono = txtTelefono.Text,
                Direccion = txtDireccion.Text,
                Salario = Convert.ToDecimal(txtSalario.Text),
                FechaContratacion = dateTimePicker1.Value
            };
            drEmpleado["Cedula"] = empleado.Cedula;
            drEmpleado["Nombres"] = empleado.Nombres;
            drEmpleado["Apellidos"] = empleado.Apellidos;
            drEmpleado["Direccion"] = empleado.Direccion;
            drEmpleado["Telefono"] = empleado.Telefono;
            drEmpleado["FechaContratacion"] = empleado.FechaContratacion;
            drEmpleado["Salario"] = empleado.Salario;
            drEmpleado["Id"] = idActualizar;

            if (daoEmpleadoImplements.Update(empleado))
            {
                MessageBox.Show("Se actualizo correctamente", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                DSNomina.Tables["Empleado"].Rows.RemoveAt(index);
                DSNomina.Tables["Empleado"].Rows.InsertAt(drEmpleado, index);
            }
            else
            {
                MessageBox.Show("No se pudo actualizar", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);

            }
            Dispose();
        }

        private void SoloTexto(object sender, KeyPressEventArgs e)
        {
            if (!char.IsLetter(e.KeyChar) && !(e.KeyChar==Convert.ToChar(8)) && !(e.KeyChar==Convert.ToChar(32)))
            {
                e.Handled = true;
                MessageBox.Show("Este campo solo acepta texto", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void SoloNumeros(object sender, KeyPressEventArgs e)
        {
            if (!char.IsDigit(e.KeyChar) && !(e.KeyChar == Convert.ToChar(8)))
            {
                e.Handled = true;
                MessageBox.Show("Este campo solo acepta numeros y no puede llevar espacios", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
    }
}
