﻿using Nomina.Business.Entities;
using Nomina.Business.Implements;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Nomina.Views
{
    public partial class FrmMain : Form
    {
        private DaoEmpleadoImplements daoEmpleadoImplements;
        private List<Empleado> empleados;
        private BindingSource bsEmpleados;
        public FrmMain()
        {
            daoEmpleadoImplements = new DaoEmpleadoImplements();
            bsEmpleados = new BindingSource();
            InitializeComponent();
        }

        private void FrmMain_Load(object sender, EventArgs e)
        {
            empleados = daoEmpleadoImplements.All();

            empleados.ForEach(ep => {
                dsNomina.Tables["Empleado"].Rows.Add(ep.EmpleadoAsArray());
            });

            bsEmpleados.DataSource = dsNomina;
            bsEmpleados.DataMember = dsNomina.Tables["Empleado"].TableName;

            dgvEmpleados.DataSource = bsEmpleados;
            dgvEmpleados.AutoGenerateColumns = true;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            FrmEmpleado frmEmpleado = new FrmEmpleado();
            frmEmpleado.DSNomina = dsNomina;
            frmEmpleado.setEnableButton(false, true);
            frmEmpleado.ShowDialog(this);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (dgvEmpleados.SelectedRows.Count >0)
            {
                int index = dgvEmpleados.SelectedRows[0].Index;

                if (dgvEmpleados.Rows[index].Cells["Id"].Value==null) {
                    MessageBox.Show("Debe seleccionar una fila o una fila no vacia", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                int id = Convert.ToInt32(dgvEmpleados.Rows[index].Cells["Id"].Value.ToString());
                string cedula = dgvEmpleados.Rows[index].Cells["Cedula"].Value.ToString();
                string nombres = dgvEmpleados.Rows[index].Cells["Nombres"].Value.ToString();
                string apellidos = dgvEmpleados.Rows[index].Cells["Apellidos"].Value.ToString();
                string direccion = dgvEmpleados.Rows[index].Cells["Direccion"].Value.ToString();
                string telefono = dgvEmpleados.Rows[index].Cells["Telefono"].Value.ToString();
                DateTime fecha = Convert.ToDateTime(dgvEmpleados.Rows[index].Cells["FechaContratacion"].Value.ToString());
                decimal salario = Convert.ToDecimal(dgvEmpleados.Rows[index].Cells["Salario"].Value.ToString());


                Empleado empleado = new Empleado()
                {
                    Id = id,
                    Nombres = nombres,
                    Apellidos = apellidos,
                    Cedula = cedula,
                    Telefono = telefono,
                    Direccion = direccion,
                    Salario = salario,
                    FechaContratacion = fecha
                };
                FrmEmpleado frmEmpleado = new FrmEmpleado();
                frmEmpleado.setEmpleado(empleado, index);
                frmEmpleado.DSNomina = dsNomina;
                frmEmpleado.setEnableButton(true, false);
                frmEmpleado.ShowDialog();

            }
            else
            {
                MessageBox.Show("Debe seleccionar una fila o una fila no vacia", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (dgvEmpleados.SelectedRows.Count > 0 )
            {

                int index = dgvEmpleados.SelectedRows[0].Index;
                if (dgvEmpleados.Rows[index].Cells["Id"].Value == null)
                {
                    MessageBox.Show("Debe seleccionar una fila o una fila no vacia", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                
                int id = Convert.ToInt32(dgvEmpleados.Rows[index].Cells["Id"].Value.ToString());
                string cedula = dgvEmpleados.Rows[index].Cells["Cedula"].Value.ToString();
                string nombres = dgvEmpleados.Rows[index].Cells["Nombres"].Value.ToString();
                string apellidos = dgvEmpleados.Rows[index].Cells["Apellidos"].Value.ToString();
                string direccion = dgvEmpleados.Rows[index].Cells["Direccion"].Value.ToString();
                string telefono = dgvEmpleados.Rows[index].Cells["Telefono"].Value.ToString();
                DateTime fecha = Convert.ToDateTime(dgvEmpleados.Rows[index].Cells["FechaContratacion"].Value.ToString());
                decimal salario = Convert.ToDecimal(dgvEmpleados.Rows[index].Cells["Salario"].Value.ToString());

                Empleado empleado = new Empleado()
                {
                    Id = id,
                    Nombres = nombres,
                    Apellidos = apellidos,
                    Cedula = cedula,
                    Telefono = telefono,
                    Direccion = direccion,
                    Salario = salario,
                    FechaContratacion = fecha
                };

                if (MessageBox.Show("Desea eliminar el registro", "Precaucion", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                {
                    dgvEmpleados.Rows.RemoveAt(index);
                    daoEmpleadoImplements.Delete(empleado);
                }
            }
            else
            {
                MessageBox.Show("Debe seleccionar una fila o una fila no vacia", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void txtBusqueda_TextChanged(object sender, EventArgs e)
        {
            DataView data = dsNomina.Tables[0].DefaultView;
            data.RowFilter = "Nombres like '%" + txtBusqueda.Text + "%'" + "or Apellidos like'%" + txtBusqueda.Text + "%'" +
                 " or Id like '%" + txtBusqueda.Text + "%'";
            dgvEmpleados.DataSource = data;
        }
    }
}
